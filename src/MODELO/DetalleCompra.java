/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MODELO;

/**
 *
 * @author Hendrick
 */
public class DetalleCompra {
    private int idComp;
    private int idProd;
    private short cantidadProd;    
    private float subtotalDetalleComp;

    public DetalleCompra() {
    }        

    public DetalleCompra(int idComp, int idProd, short cantidadProd,
            float subtotalDetalleComp) {
        this.idComp = idComp;
        this.cantidadProd = cantidadProd;        
        this.subtotalDetalleComp = subtotalDetalleComp;
    }

    public int getIdComp() {
        return idComp;
    }

    public void setIdComp(int idComp) {
        this.idComp = idComp;
    }

    public int getidProd() {
        return idProd;
    }

    public void setidProd(int idProd) {
        this.idProd = idProd;
    }

    public short getCantidadProd() {
        return cantidadProd;
    }

    public void setCantidadProd(short cantidadProd) {
        this.cantidadProd = cantidadProd;
    }
    
    public float getSubtotalDetalleComp() {
        return subtotalDetalleComp;
    }

    public void setSubtotalDetalleComp(float subtotalDetalleComp) {
        this.subtotalDetalleComp = subtotalDetalleComp;
    }

    @Override
    public String toString() {
        return "DetalleCompra{" + "idComp=" + idComp + ", idProd=" + 
                idProd + ", cantidadProd=" + cantidadProd + ", "
                + "subtotalDetalleComp=" + subtotalDetalleComp + '}';
    }
    
    
}
