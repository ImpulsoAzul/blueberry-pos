/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MODELO;

/**
 *
 * @author Hendrick
 */
public class Emisor {
    private String idEmisor;
    private String nombreComercialEmisor;
    private String denominacionSocialEmisor;
    private String direccionEmisor;
    private String representanteEmisor;
    private String nombreMuni;
    private String numeroResolucionSistema;
    private String fechaResolucionSistema;
    private String fechaCreacionEmisor;
    private String fechaExpiraEmisor;
    private String estadoEmisor;

    public Emisor() {
    }

    public Emisor(String idEmisor, String nombreComercialEmisor, String denominacionSocialEmisor, String direccionEmisor, String representanteEmisor, String nombreMuni, String numeroResolucionSistema, String fechaResolucionSistema, String fechaCreacionEmisor, String fechaExpiraEmisor, String estadoEmisor) {
        this.idEmisor = idEmisor;
        this.nombreComercialEmisor = nombreComercialEmisor;
        this.denominacionSocialEmisor = denominacionSocialEmisor;
        this.direccionEmisor = direccionEmisor;
        this.representanteEmisor = representanteEmisor;
        this.nombreMuni = nombreMuni;
        this.numeroResolucionSistema = numeroResolucionSistema;
        this.fechaResolucionSistema = fechaResolucionSistema;
        this.fechaCreacionEmisor = fechaCreacionEmisor;
        this.fechaExpiraEmisor = fechaExpiraEmisor;
        this.estadoEmisor = estadoEmisor;
    }

    public String getEstadoEmisor() {
        return estadoEmisor;
    }

    public void setEstadoEmisor(String estadoEmisor) {
        this.estadoEmisor = estadoEmisor;
    }

    public String getIdEmisor() {
        return idEmisor;
    }

    public void setIdEmisor(String idEmisor) {
        this.idEmisor = idEmisor;
    }

    public String getNombreComercialEmisor() {
        return nombreComercialEmisor;
    }

    public void setNombreComercialEmisor(String nombreComercialEmisor) {
        this.nombreComercialEmisor = nombreComercialEmisor;
    }

    public String getDenominacionSocialEmisor() {
        return denominacionSocialEmisor;
    }

    public void setDenominacionSocialEmisor(String denominacionSocialEmisor) {
        this.denominacionSocialEmisor = denominacionSocialEmisor;
    }

    public String getDireccionEmisor() {
        return direccionEmisor;
    }

    public void setDireccionEmisor(String direccionEmisor) {
        this.direccionEmisor = direccionEmisor;
    }

    public String getRepresentanteEmisor() {
        return representanteEmisor;
    }

    public void setRepresentanteEmisor(String representanteEmisor) {
        this.representanteEmisor = representanteEmisor;
    }

    public String getNombreMuni() {
        return nombreMuni;
    }

    public void setNombreMuni(String nombreMuni) {
        this.nombreMuni = nombreMuni;
    }

    public String getNumeroResolucionSistema() {
        return numeroResolucionSistema;
    }

    public void setNumeroResolucionSistema(String numeroResolucionSistema) {
        this.numeroResolucionSistema = numeroResolucionSistema;
    }

    public String getFechaResolucionSistema() {
        return fechaResolucionSistema;
    }

    public void setFechaResolucionSistema(String fechaResolucionSistema) {
        this.fechaResolucionSistema = fechaResolucionSistema;
    }

    public String getFechaCreacionEmisor() {
        return fechaCreacionEmisor;
    }

    public void setFechaCreacionEmisor(String fechaCreacionEmisor) {
        this.fechaCreacionEmisor = fechaCreacionEmisor;
    }

    public String getFechaExpiraEmisor() {
        return fechaExpiraEmisor;
    }

    public void setFechaExpiraEmisor(String fechaExpiraEmisor) {
        this.fechaExpiraEmisor = fechaExpiraEmisor;
    }

    @Override
    public String toString() {
        return "Emisor{" + "idEmisor=" + idEmisor + ", nombreComercialEmisor=" 
                + nombreComercialEmisor + ", denominacionSocialEmisor=" 
                + denominacionSocialEmisor + ", direccionEmisor=" 
                + direccionEmisor + ", representanteEmisor=" 
                + representanteEmisor + ", nombreMuni=" + nombreMuni 
                + ", numeroResolucionSistema=" + numeroResolucionSistema 
                + ", fechaResolucionSistema=" + fechaResolucionSistema 
                + ", fechaCreacionEmisor=" + fechaCreacionEmisor 
                + ", fechaExpiraEmisor=" + fechaExpiraEmisor + ", estadoEmisor=" 
                + estadoEmisor + '}';
    }                
    
}
