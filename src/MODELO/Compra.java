/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MODELO;

/**
 *
 * @author Hendrick
 */
public class Compra {
    private String nitProv;
    private String serieFac;
    private int numeroFac;
    private String fechaComp;
    private float totalComp;
    private String fechaRegistroComp;
    private String tipoComp;
    private String estadoComp;

    public Compra() {
    }

    public Compra(String nitProv, String serieFac, int numeroFac, 
            String fechaComp, float totalComp, String fechaRegistroComp, 
            String tipoComp, String estadoComp) {
        this.nitProv = nitProv;
        this.serieFac = serieFac;
        this.numeroFac = numeroFac;
        this.fechaComp = fechaComp;
        this.totalComp = totalComp;
        this.tipoComp = tipoComp;
        this.fechaRegistroComp = fechaRegistroComp;
        
        this.estadoComp = estadoComp;
    }

    public String getNitProv() {
        return nitProv;
    }

    public void setNitProv(String nitProv) {
        this.nitProv = nitProv;
    }

    public String getSerieFac() {
        return serieFac;
    }

    public void setSerieFac(String serieFac) {
        this.serieFac = serieFac;
    }

    public int getNumeroFac() {
        return numeroFac;
    }

    public void setNumeroFac(int numeroFac) {
        this.numeroFac = numeroFac;
    }

    public String getFechaComp() {
        return fechaComp;
    }

    public void setFechaComp(String fechaComp) {
        this.fechaComp = fechaComp;
    }

    public float getTotalComp() {
        return totalComp;
    }

    public void setTotalComp(float totalComp) {
        this.totalComp = totalComp;
    }

    public String getFechaRegistroComp() {
        return fechaRegistroComp;
    }

    public void setFechaRegistroComp(String fechaRegistroComp) {
        this.fechaRegistroComp = fechaRegistroComp;
    }

    public String getTipoComp() {
        return tipoComp;
    }

    public void setTipoComp(String tipoComp) {
        this.tipoComp = tipoComp;
    }
       
    
    public String getEstadoComp() {
        return estadoComp;
    }

    public void setEstadoComp(String estadoComp) {
        this.estadoComp = estadoComp;
    }
    
    public void clear(){
        this.nitProv = null;
        this.serieFac = null;
        this.numeroFac = 0;
        this.fechaComp = null;
        this.totalComp = 0;
        this.fechaRegistroComp = null;
        this.tipoComp = null;
        this.estadoComp = null;
    }
    
    
    @Override
    public String toString() {
        return "Compra{" + "nitProv=" + nitProv + ", serieFac=" + serieFac + ", numeroFac=" + numeroFac + ", fechaComp=" + fechaComp + ", totalComp=" + totalComp + ", fechaRegistroComp=" + fechaRegistroComp + ", estadoComp=" + estadoComp + '}';
    }
    
    
    
}
