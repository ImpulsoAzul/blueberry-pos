/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MODELO;

/**
 *
 * @author Hendrick
 */
public class Inventario {  
   private int idProd; 
   private String descripcionProd;
   private String codigoBarraProd;
   private String nombreMar;
   private String nombreTipoPro;
   private boolean devolucionProd;
   private float precioProd;
   private float precioMayoristaProd;
   private float descuentoProd;
   private int existenciaProd;
   private String estadoProd;

    public Inventario() {
    }

    public Inventario(String descripcionProd, String codigoBarraProd, 
            String nombreMar, String nombreTipoPro, boolean devolucionProd, 
            float precioProd, float precioMayoristaProd, float descuentoProd, 
            int existenciaProd, String estadoProd) {
        this.descripcionProd = descripcionProd;
        this.codigoBarraProd = codigoBarraProd;
        this.nombreMar = nombreMar;
        this.nombreTipoPro = nombreTipoPro;
        this.devolucionProd = devolucionProd;
        this.precioProd = precioProd;
        this.precioMayoristaProd = precioMayoristaProd;
        this.descuentoProd = descuentoProd;
        this.existenciaProd = existenciaProd;
        this.estadoProd = estadoProd;
    }

    public String getDescripcionProd() {
        return descripcionProd;
    }

    public void setDescripcionProd(String descripcionProd) {
        this.descripcionProd = descripcionProd;
    }

    public String getCodigoBarraProd() {
        return codigoBarraProd;
    }

    public void setCodigoBarraProd(String codigoBarraProd) {
        this.codigoBarraProd = codigoBarraProd;
    }

    public String getNombreMar() {
        return nombreMar;
    }

    public void setNombreMar(String nombreMar) {
        this.nombreMar = nombreMar;
    }

    public String getNombreTipoPro() {
        return nombreTipoPro;
    }

    public void setNombreTipoPro(String nombreTipoPro) {
        this.nombreTipoPro = nombreTipoPro;
    }

    public boolean isDevolucionProd() {
        return devolucionProd;
    }

    public void setDevolucionProd(boolean devolucionProd) {
        this.devolucionProd = devolucionProd;
    }

    public float getPrecioProd() {
        return precioProd;
    }

    public void setPrecioProd(float precioProd) {
        this.precioProd = precioProd;
    }

    public float getPrecioMayoristaProd() {
        return precioMayoristaProd;
    }

    public void setPrecioMayoristaProd(float precioMayoristaProd) {
        this.precioMayoristaProd = precioMayoristaProd;
    }

    public float getDescuentoProd() {
        return descuentoProd;
    }

    public void setDescuentoProd(float descuentoProd) {
        this.descuentoProd = descuentoProd;
    }

    public int getExistenciaProd() {
        return existenciaProd;
    }

    public void setExistenciaProd(int existenciaProd) {
        this.existenciaProd = existenciaProd;
    }

    public String getEstadoProd() {
        return estadoProd;
    }

    public void setEstadoProd(String estadoProd) {
        this.estadoProd = estadoProd;
    }

    public int getIdProd() {
        return idProd;
    }

    public void setIdProd(int idProd) {
        this.idProd = idProd;
    }
    
    

    @Override
    public String toString() {
        return "Inventario{" + "descripcionProd=" + descripcionProd + ", "
                + "codigoBarraProd=" + codigoBarraProd + ", nombreMar=" + 
                nombreMar + ", nombreTipoPro=" + nombreTipoPro + ", "
                + "devolucionProd=" + devolucionProd + ", precioProd=" + 
                precioProd + ", precioMayoristaProd=" + precioMayoristaProd + 
                ", descuentoProd=" + descuentoProd + ", existenciaProd=" + 
                existenciaProd + ", estadoProd=" + estadoProd + '}';
    }

    
    
    
}