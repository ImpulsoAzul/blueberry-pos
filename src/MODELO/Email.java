/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MODELO;

/**
 *
 * @author Hendrick
 */
public class Email {
    private String email;
    private String nitProv;

    public Email() {
    }

    public Email(String email, String nitProv) {
        this.email = email;
        this.nitProv = nitProv;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNitProv() {
        return nitProv;
    }

    public void setNitProv(String nitProv) {
        this.nitProv = nitProv;
    }

    @Override
    public String toString() {
        return "Email{" + "email=" + email + ", nitProv=" + nitProv + '}';
    }
    
    
    
    
}
