/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MODELO;

/**
 *
 * @author Hendrick
 */
public class TipoProducto {
    private String nombreTipoPro;
    private String descripcionTipoPro;

    public TipoProducto() {
    }

    public TipoProducto(String nombreTipoPro, String descripcionTipoPro) {
        this.nombreTipoPro = nombreTipoPro;
        this.descripcionTipoPro = descripcionTipoPro;
    }

    public String getNombreTipoPro() {
        return nombreTipoPro;
    }

    public void setNombreTipoPro(String nombreTipoPro) {
        this.nombreTipoPro = nombreTipoPro;
    }

    public String getDescripcionTipoPro() {
        return descripcionTipoPro;
    }

    public void setDescripcionTipoPro(String descripcionTipoPro) {
        this.descripcionTipoPro = descripcionTipoPro;
    }

    @Override
    public String toString() {
        return "TipoProducto{" + "nombreTipoPro=" + nombreTipoPro + ", descripcionTipoPro=" + descripcionTipoPro + '}';
    }
    
    
    
}
