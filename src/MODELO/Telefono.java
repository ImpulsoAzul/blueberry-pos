/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MODELO;

/**
 *
 * @author Hendrick
 */
public class Telefono {
    private String numeroTel;
    private String nitProv;

    public Telefono() {
    }

    public Telefono(String numeroTel, String nitProv) {
        this.numeroTel = numeroTel;
        this.nitProv = nitProv;
    }

    public String getNumeroTel() {
        return numeroTel;
    }

    public void setNumeroTel(String numeroTel) {
        this.numeroTel = numeroTel;
    }

    public String getNitProv() {
        return nitProv;
    }

    public void setNitProv(String nitProv) {
        this.nitProv = nitProv;
    }

    @Override
    public String toString() {
        return "Telefono{" + "numeroTel=" + numeroTel + ", nitProv=" + nitProv + '}';
    }
    
    
}
