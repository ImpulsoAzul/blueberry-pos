/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MODELO;

/**
 *
 * @author Hendrick
 */
public class Resolucion {
    private int idResolucion;
    private String nombreDoc;
    private String idEmisor;
    private String numeroResolucion;
    private String fechaResolucion;
    private String serieResolucion;
    private int delResolucion;
    private int alResolucion;
    private String estadoResolucion;

    public Resolucion() {
    }

    public Resolucion(String nombreDoc, String idEmisor, 
            String numeroResolucion, String fechaResolucion, 
            String serieResolucion, int delResolucion, int alResolucion, 
            String estadoResolucion) {
        this.nombreDoc = nombreDoc;
        this.idEmisor = idEmisor;
        this.numeroResolucion = numeroResolucion;
        this.fechaResolucion = fechaResolucion;
        this.serieResolucion = serieResolucion;
        this.delResolucion = delResolucion;
        this.alResolucion = alResolucion;
        this.estadoResolucion = estadoResolucion;
    }

    public String getEstadoResolucion() {
        return estadoResolucion;
    }

    public void setEstadoResolucion(String estadoResolucion) {
        this.estadoResolucion = estadoResolucion;
    }

    public int getIdResolucion() {
        return idResolucion;
    }

    public void setIdResolucion(int idResolucion) {
        this.idResolucion = idResolucion;
    }

    public String getNombreDoc() {
        return nombreDoc;
    }

    public void setNombreDoc(String nombreDoc) {
        this.nombreDoc = nombreDoc;
    }

    public String getIdEmisor() {
        return idEmisor;
    }

    public void setIdEmisor(String idEmisor) {
        this.idEmisor = idEmisor;
    }

    public String getNumeroResolucion() {
        return numeroResolucion;
    }

    public void setNumeroResolucion(String numeroResolucion) {
        this.numeroResolucion = numeroResolucion;
    }

    public String getFechaResolucion() {
        return fechaResolucion;
    }

    public void setFechaResolucion(String fechaResolucion) {
        this.fechaResolucion = fechaResolucion;
    }

    public String getSerieResolucion() {
        return serieResolucion;
    }

    public void setSerieResolucion(String serieResolucion) {
        this.serieResolucion = serieResolucion;
    }

    public int getDelResolucion() {
        return delResolucion;
    }

    public void setDelResolucion(int delResolucion) {
        this.delResolucion = delResolucion;
    }

    public int getAlResolucion() {
        return alResolucion;
    }

    public void setAlResolucion(int alResolucion) {
        this.alResolucion = alResolucion;
    }

    @Override
    public String toString() {
        return "Resolucion{" + "idResolucion=" + idResolucion + ", nombreDoc=" 
                + nombreDoc + ", idEmisor=" + idEmisor + ", numeroResolucion=" 
                + numeroResolucion + ", fechaResolucion=" + fechaResolucion 
                + ", serieResolucion=" + serieResolucion + ", delResolucion=" 
                + delResolucion + ", alResolucion=" + alResolucion 
                + ", estadoResolucion=" + estadoResolucion + '}';
    }
    
    
    
}
