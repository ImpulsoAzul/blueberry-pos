/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MODELO;

/**
 *
 * @author Hendrick
 */
public class Proveedor {
    private String nitProv;
    private String nombreProv;
    private String direccionProv;
    private String nombreMuni;

    public Proveedor() {
    }

    public Proveedor(String nitProv, String nombreProv, String direccionProv) {
        this.nitProv = nitProv;
        this.nombreProv = nombreProv;
        this.direccionProv = direccionProv;
    }

    public String getNitProv() {
        return nitProv;
    }

    public void setNitProv(String nitProv) {
        this.nitProv = nitProv;
    }

    public String getNombreProv() {
        return nombreProv;
    }

    public void setNombreProv(String nombreProv) {
        this.nombreProv = nombreProv;
    }

    public String getDireccionProv() {
        return direccionProv;
    }

    public void setDireccionProv(String direccionProv) {
        this.direccionProv = direccionProv;
    }

    public String getNombreMuni() {
        return nombreMuni;
    }

    public void setNombreMuni(String nombreMuni) {
        this.nombreMuni = nombreMuni;
    }
    
    

    @Override
    public String toString() {
        return "Proveedor{" + "nitProv=" + nitProv + ", nombreProv=" + nombreProv + ", direccionProv=" + direccionProv + '}';
    }
    
    
}
