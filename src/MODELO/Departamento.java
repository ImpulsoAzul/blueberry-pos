/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MODELO;

/**
 *
 * @author Hendrick
 */
public class Departamento {
    private String nombreDepto;

    public Departamento() {
    }

    public Departamento(String nombreDepto) {
        this.nombreDepto = nombreDepto;
    }

    public String getNombreDepto() {
        return nombreDepto;
    }

    public void setNombreDepto(String nombreDepto) {
        this.nombreDepto = nombreDepto;
    }

    @Override
    public String toString() {
        return "Departamento{" + "nombreDepto=" + nombreDepto + '}';
    }
    
    
}
