/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MODELO;

/**
 *
 * @author Hendrick
 */
public class Documento {
    private String nombreDoc;
    private String descripcionDoc;

    public Documento() {
    }

    public Documento(String nombreDoc, String descripcionDoc) {
        this.nombreDoc = nombreDoc;
        this.descripcionDoc = descripcionDoc;
    }

    public String getNombreDoc() {
        return nombreDoc;
    }

    public void setNombreDoc(String nombreDoc) {
        this.nombreDoc = nombreDoc;
    }

    public String getDescripcionDoc() {
        return descripcionDoc;
    }

    public void setDescripcionDoc(String descripcionDoc) {
        this.descripcionDoc = descripcionDoc;
    }

    @Override
    public String toString() {
        return "Documento{" + "nombreDoc=" + nombreDoc + ", descripcionDoc=" + descripcionDoc + '}';
    }
    
    
    
}
