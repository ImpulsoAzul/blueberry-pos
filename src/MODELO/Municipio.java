/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MODELO;

/**
 *
 * @author Hendrick
 */
public class Municipio {
    private String nombreMuni;
    private String nombreDepto;

    public Municipio() {
    }

    public Municipio(String nombreMuni, String nombreDepto) {
        this.nombreMuni = nombreMuni;
        this.nombreDepto = nombreDepto;
    }

    public String getNombreMuni() {
        return nombreMuni;
    }

    public void setNombreMuni(String nombreMuni) {
        this.nombreMuni = nombreMuni;
    }

    public String getNombreDepto() {
        return nombreDepto;
    }

    public void setNombreDepto(String nombreDepto) {
        this.nombreDepto = nombreDepto;
    }

    @Override
    public String toString() {
        return "Municipio{" + "nombreMuni=" + nombreMuni + ", nombreDepto=" + nombreDepto + '}';
    }
    
    
}
