/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MODELO;

/**
 *
 * @author Hendrick
 */
public class Marca {
    private String nombremarca;
    private boolean estadomarca;

    public Marca() {
    }

    public Marca(String nombremarca) {
        this.nombremarca = nombremarca;        
    }

    public boolean isEstadomarca() {
        return estadomarca;
    }

    public void setEstadomarca(boolean estadomarca) {
        this.estadomarca = estadomarca;
    }
    

    public String getNombremarca() {
        return nombremarca;
    }

    public void setNombremarca(String nombremarca) {
        this.nombremarca = nombremarca;
    }

    @Override
    public String toString() {
        return "Marca{" + "nombreMar=" + nombremarca + '}';
    }
    
    
    
}
