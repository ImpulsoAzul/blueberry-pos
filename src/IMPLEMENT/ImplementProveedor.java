/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IMPLEMENT;

import INTERFACES.InterfaceProveedor;
import MODELO.Email;
import MODELO.Proveedor;
import MODELO.Telefono;
import java.awt.HeadlessException;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Hendrick
 */
public class ImplementProveedor implements InterfaceProveedor, Serializable{
    private final String SQLC = "insert into proveedor (nitProv, nombreProv, direccionProv, nombreMuni) "
            + "values (?,?,?,?) ";
    private final String SQLCT = "insert into telefono (numeroTel, nitProv) values (?,?)";
    private final String SQLCE = "insert into email (email, nitProv) values (?,?)";
    private final String SQLR = "select * from proveedor";
    private final String SQLRID = "select * from proveedor where nitProv = ?";
    private final String SQLU = "update proveedor set nitProv = ?, "
            + "nombreProv = ?, direccionProv= ?, nombreMuni= ? where nitProv= ?";
    private final String SQLD = "delete from proveedor where nitProv = ?";
    private final String SQLDTEL = "delete from telefono where nitProv = ?";
    private final String SQLDEMAIL = "delete from email where nitProv = ?";
    private ResultSet select;

    public ImplementProveedor() {
    }

    @Override
    public void insert(Proveedor proveedor, List<Telefono> telefonos, List<Email> correos) {
        Conexion miConexion = new Conexion();
        try{            
            PreparedStatement ps;            
            miConexion.getEnlace().setAutoCommit(false);
            ps = miConexion.getEnlace().prepareStatement(SQLC);
            //insertando primero el proveedor
            ps.setString(1, proveedor.getNitProv());
            ps.setString(2, proveedor.getNombreProv());
            ps.setString(3, proveedor.getDireccionProv());
            ps.setString(4, proveedor.getNombreMuni());
            ps.executeUpdate();
            //insertando los números de telefono
            for (Telefono telefono : telefonos) {
                ps = miConexion.getEnlace().prepareStatement(SQLCT);    
                ps.setString(1, telefono.getNumeroTel());
                ps.setString(2, telefono.getNitProv());
                ps.executeUpdate();
            }
            
            for (Email correo : correos) {
                ps = miConexion.getEnlace().prepareStatement(SQLCE);    
                ps.setString(1, correo.getEmail());
                ps.setString(2, correo.getNitProv());
                ps.executeUpdate();
            }            
            //insertando los correos electrónicos
            miConexion.getEnlace().commit();
            JOptionPane.showMessageDialog(null, "Registros almacenados exitosamente", "Información", 1);
        }catch(SQLException | HeadlessException e){
            try {
                miConexion.getEnlace().rollback();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
            JOptionPane.showMessageDialog(null, "Ocurrió un error, contacte al administrador\n"+e.toString(), "Error", 2);
        }finally{
            try {
                miConexion.close();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
        } 
    }
       
    @Override
    public void select() {
        Conexion miConexion = new Conexion();
        try{
            PreparedStatement ps;            
            ps = miConexion.getEnlace().prepareStatement(SQLR);
            select = ps.executeQuery();            
        }catch(SQLException | HeadlessException e){
            try {
                miConexion.getEnlace().rollback();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
            JOptionPane.showMessageDialog(null, "Ocurrió un error, contacte al administrador\n"+e.toString(), "Error", 2);        
        }
    }
                    
    @Override
    public void update(Proveedor proveedor, String id, List<Telefono> telefonos,List<Email> correos) {
        Conexion miConexion = new Conexion();
        Conexion miConexionD = new Conexion();
        try{
            PreparedStatement ps;            
            PreparedStatement psd; 
             //-------------------------------------------------------//
            miConexionD.getEnlace().setAutoCommit(false);
            psd = miConexionD.getEnlace().prepareStatement(SQLDTEL);
            psd.setString(1, id);
            psd.executeUpdate();
            psd = miConexionD.getEnlace().prepareStatement(SQLDEMAIL);
            psd.setString(1, id);
            psd.executeUpdate();
            miConexionD.getEnlace().commit();
            //-------------------------------------------------------//
            miConexion.getEnlace().setAutoCommit(false);
            ps = miConexion.getEnlace().prepareStatement(SQLU);
            //insertando primero el proveedor                         
            ps.setString(1, proveedor.getNitProv());
            ps.setString(2, proveedor.getNombreProv());
            ps.setString(3, proveedor.getDireccionProv());
            ps.setString(4, proveedor.getNombreMuni());
            ps.setString(5, id);
            ps.executeUpdate();
           
            //insertando los números de telefono            
            for (Telefono telefono : telefonos) {
                ps = miConexion.getEnlace().prepareStatement(SQLCT);    
                ps.setString(1, telefono.getNumeroTel());
                ps.setString(2, proveedor.getNitProv());
                ps.executeUpdate();
            }
            //-------------------------------------------------------//
       
            //-------------------------------------------------------//
            for (Email correo : correos) {
                ps = miConexion.getEnlace().prepareStatement(SQLCE);    
                ps.setString(1, correo.getEmail());
                ps.setString(2, proveedor.getNitProv());
                ps.executeUpdate();
            }
            miConexion.getEnlace().commit();
            JOptionPane.showMessageDialog(null, "Registros actualizados exitosamente", "Información", 1);
        }catch(SQLException | HeadlessException e){
            try {
                miConexion.getEnlace().rollback();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
            JOptionPane.showMessageDialog(null, "Ocurrió un error, contacte al administrador\n"+e.toString(), "Error", 2);
        }finally{
            try {
                miConexion.close();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void delete(String dato) {
         Conexion miConexion = new Conexion();
        try{            
            PreparedStatement ps;            
            miConexion.getEnlace().setAutoCommit(false);
            ps = miConexion.getEnlace().prepareStatement(SQLD);
            ps.setString(1, dato);            
            ps.executeUpdate();
            miConexion.getEnlace().commit();
            JOptionPane.showMessageDialog(null, "Registro eliminado exitosamente", "Información", 1);
        }catch(SQLException | HeadlessException e){
            try {
                miConexion.getEnlace().rollback();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
            JOptionPane.showMessageDialog(null, "Ocurrió un error, contacte al administrador\n"+e.toString(), "Error", 2);
        }finally{
            try {
                miConexion.close();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
       
    @Override
    public void selectId(String codigo) {
        Conexion miConexion = new Conexion();
        try{
            PreparedStatement ps;            
            ps = miConexion.getEnlace().prepareStatement(SQLRID);
            ps.setString(1, codigo);
            select = ps.executeQuery();            
        }catch(SQLException | HeadlessException e){
            try {
                miConexion.getEnlace().rollback();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
            JOptionPane.showMessageDialog(null, "Ocurrió un error, contacte al administrador\n"+e.toString(), "Error", 2);        
        }
    }    
    
    public ResultSet getSelect(){
        return select;
    }
}
   

