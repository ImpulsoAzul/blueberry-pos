/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IMPLEMENT;

import INTERFACES.InterfaceEmail;
import MODELO.Email;
import java.awt.HeadlessException;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Hendrick
 */
public class ImplementEmail implements InterfaceEmail, Serializable{
    private final String SQLC = "insert into email (email, nitProv) "
            + "values (?,?) ";
    private final String SQLR = "select * from email";
    private final String SQLRID = "select email from email where nitProv = ?";
    private final String SQLU = "update documento set nombreDoc= ?, "
            + "descripcionDoc= ? where nombredoc= ?";
    private final String SQLD = "delete from email where nitProv = ?";
    private ResultSet select;

    public ImplementEmail() {
    }

    @Override
    public void insert(Email email) {
        Conexion miConexion = new Conexion();
        try{            
            PreparedStatement ps;            
            miConexion.getEnlace().setAutoCommit(false);
            ps = miConexion.getEnlace().prepareStatement(SQLC);
           // ps.setString(1, documento.getNombreDoc());
           // ps.setString(2, documento.getDescripcionDoc());
            ps.executeUpdate();
            miConexion.getEnlace().commit();
            JOptionPane.showMessageDialog(null, "Registro almacenado exitosamente", "Información", 1);
        }catch(SQLException | HeadlessException e){
            try {
                miConexion.getEnlace().rollback();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
            JOptionPane.showMessageDialog(null, "Ocurrió un error, contacte al administrador\n"+e.toString(), "Error", 2);
        }finally{
            try {
                miConexion.close();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
        }                
    }

    @Override
    public void select() {
        Conexion miConexion = new Conexion();
        try{
            PreparedStatement ps;            
            ps = miConexion.getEnlace().prepareStatement(SQLR);
            select = ps.executeQuery();            
        }catch(SQLException | HeadlessException e){
            try {
                miConexion.getEnlace().rollback();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
            JOptionPane.showMessageDialog(null, "Ocurrió un error, contacte al administrador\n"+e.toString(), "Error", 2);        
        }
    }
                    
    @Override
    public void update(Email email, String id) {
         Conexion miConexion = new Conexion();
        try{            
            PreparedStatement ps;            
            miConexion.getEnlace().setAutoCommit(false);
            ps = miConexion.getEnlace().prepareStatement(SQLU);
           // ps.setString(1, documento.getNombreDoc());
           // ps.setString(2, documento.getDescripcionDoc());
            ps.setString(3, id);
            ps.executeUpdate();
            miConexion.getEnlace().commit();
            JOptionPane.showMessageDialog(null, "Registro actualizado exitosamente", "Información", 1);
        }catch(SQLException | HeadlessException e){
            try {
                miConexion.getEnlace().rollback();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
            JOptionPane.showMessageDialog(null, "Ocurrió un error, contacte al administrador\n"+e.toString(), "Error", 2);
        }finally{
            try {
                miConexion.close();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void delete(String dato) {
         Conexion miConexion = new Conexion();
        try{            
            PreparedStatement ps;            
            miConexion.getEnlace().setAutoCommit(false);
            ps = miConexion.getEnlace().prepareStatement(SQLD);
            ps.setString(1, dato);            
            ps.executeUpdate();
            miConexion.getEnlace().commit();
            //JOptionPane.showMessageDialog(null, "Registro eliminado exitosamente", "Información", 1);
        }catch(SQLException | HeadlessException e){
            try {
                miConexion.getEnlace().rollback();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
            JOptionPane.showMessageDialog(null, "Ocurrió un error, contacte al administrador\n"+e.toString(), "Error", 2);
        }finally{
            try {
                miConexion.close();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
       
    @Override
    public void selectId(String codigo) {
        Conexion miConexion = new Conexion();
        try{
            PreparedStatement ps;            
            ps = miConexion.getEnlace().prepareStatement(SQLRID);
            ps.setString(1, codigo);
            select = ps.executeQuery();            
        }catch(SQLException | HeadlessException e){
            try {
                miConexion.getEnlace().rollback();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
            JOptionPane.showMessageDialog(null, "Ocurrió un error, contacte al administrador\n"+e.toString(), "Error", 2);        
        }
    }    
    
    public ResultSet getSelect(){
        return select;
    }
    
}
