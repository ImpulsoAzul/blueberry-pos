/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IMPLEMENT;

import INTERFACES.InterfaceInventario;
import MODELO.Inventario;
import java.awt.HeadlessException;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Hendrick
 */
public class ImplementInventario implements InterfaceInventario, Serializable{    
    private final String SQLC = "insert into inventario (descripcionProd, "
        + "codigoBarraProd, nombreMar, nombreTipoPro, devolucionProd, "
        + "precioProd, precioMayoristaProd, descuentoProd, existenciaProd, "
        + "estadoProd ) values (?,?,?,?,?,?,?,?,?,?)";
    private final String SQLR = "select * from inventario";
    private final String SQLLIKE = "select * from inventario where descripcionProd like ?";
    private final String SQLRID = "select * from inventario where idProd = ?";
    private final String SQLRBARRA = "select * from inventario where codigoBarraProd = ?";
    private final String SQLU = "update inventario set descripcionProd=?, "
            + "codigoBarraProd=?, nombreMar=?, nombreTipoPro=?, "
            + "devolucionProd=?, precioProd=?, precioMayoristaProd=?, "
            + "descuentoProd=? where idProd = ?";
    private final String SQLD = "delete from inventario where codigoProd = ?";
    private final String SQLR_AVANZADO = "SELECT * FROM inventario where "
            + "(estadoProd = ?) AND (descripcionProd like ?) AND "
            + "(codigoBarraProd like ?) AND (existenciaProd BETWEEN ? AND ?) AND "
            + "(precioProd BETWEEN ? and ?)";
    private final String SQLU_ACTIVAR_DESACTIVAR = "update inventario set estadoProd = ? "
            + "where idProd = ?";
    private final String SQLU_CAMBIAR_DESCUENTO = "update inventario set descuentoProd = ? "
            + "where idProd = ?";
    private ResultSet select;
    public static final int MYSQL_DUPLICATE_PK = 1062;

    public ImplementInventario() {
    }

    @Override
    public void insert(Inventario inventario) {
        Conexion miConexion = new Conexion();
        try{            
            PreparedStatement ps;            
            miConexion.getEnlace().setAutoCommit(false);
            ps = miConexion.getEnlace().prepareStatement(SQLC);
            ps.setString(1, inventario.getDescripcionProd());
            ps.setString(2, inventario.getCodigoBarraProd());
            ps.setString(3, inventario.getNombreMar());
            ps.setString(4, inventario.getNombreTipoPro());
            ps.setBoolean(5, inventario.isDevolucionProd());
            ps.setFloat(6, inventario.getPrecioProd());
            ps.setFloat(7, inventario.getPrecioMayoristaProd());
            ps.setFloat(8, inventario.getDescuentoProd());
            ps.setInt(9, inventario.getExistenciaProd());
            ps.setString(10, "A");            
            ps.executeUpdate();
            miConexion.getEnlace().commit();
            JOptionPane.showMessageDialog(null, "Registro almacenado exitosamente", "Información", 1);
        }catch(SQLException e){
            try {
                miConexion.getEnlace().rollback();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            } 
            if (e.getErrorCode() == MYSQL_DUPLICATE_PK){
                JOptionPane.showMessageDialog(null, "Ocurrió un error:\nYa existe en la base de datos un producto \ncon el código de barras: "+inventario.getCodigoBarraProd(), "Información", 0);
            }else{
                JOptionPane.showMessageDialog(null, "Ocurrió un error:\n" +e.toString()+"\nContacte al administrador", "Error", 0);
            }
        }finally{
            try {
                miConexion.close();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void select() {
         Conexion miConexion = new Conexion();
        try{
            PreparedStatement ps;            
            ps = miConexion.getEnlace().prepareStatement(SQLR);
            select = ps.executeQuery();            
        }catch(SQLException | HeadlessException e){
            try {
                miConexion.getEnlace().rollback();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
            JOptionPane.showMessageDialog(null, "Ocurrió un error, contacte al administrador\n"+e.toString(), "Error", 2);        
        }
    }

    @Override
    public void selectId(int idProd) {
           Conexion miConexion = new Conexion();
        try{
            //int id = Integer.parseInt(codigo);
            PreparedStatement ps;            
            ps = miConexion.getEnlace().prepareStatement(SQLRID);
            ps.setInt(1, idProd);
            select = ps.executeQuery();            
        }catch(SQLException | HeadlessException e){
            try {
                miConexion.getEnlace().rollback();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
            JOptionPane.showMessageDialog(null, "Ocurrió un error, contacte al administrador\n"+e.toString(), "Error", 2);        
        }    
    }
    
    @Override
    public void selectLike(String descripcionProd) {
           Conexion miConexion = new Conexion();
        try{            
            PreparedStatement ps;            
            ps = miConexion.getEnlace().prepareStatement(SQLLIKE);
            ps.setString(1, '%'+descripcionProd+'%');            
            select = ps.executeQuery();            
        }catch(SQLException | HeadlessException e){            
            JOptionPane.showMessageDialog(null, "Ocurrió un error, contacte al administrador\n"+e.toString(), "Error", 2);        
        }    
    }
    
    @Override
    public void selectAvanzado(String descripcionProd, String codigoBarraProd, 
            float precioProdMin, float precioProdMax, int existenciaProdMin, 
            int existenciaProdMax, String estadoProd) {
           Conexion miConexion = new Conexion();
        try{            
            PreparedStatement ps;            
            ps = miConexion.getEnlace().prepareStatement(SQLR_AVANZADO);
            ps.setString(1, estadoProd);
            ps.setString(2, '%'+descripcionProd+'%');
            ps.setString(3, '%'+codigoBarraProd+'%');
            ps.setInt(4, existenciaProdMin);
            ps.setInt(5, existenciaProdMax);
            ps.setFloat(6, precioProdMin);
            ps.setFloat(7, precioProdMax);            
            select = ps.executeQuery();            
        }catch(SQLException | HeadlessException e){            
            JOptionPane.showMessageDialog(null, "Ocurrió un error, contacte al administrador\n"+e.toString(), "Error", 2);        
        }    
    }
    
    @Override
    public void selectBarra(String codigoBarraProd) {
        Conexion miConexion = new Conexion();
        try{            
            PreparedStatement ps;            
            ps = miConexion.getEnlace().prepareStatement(SQLRBARRA);
            ps.setString(1, codigoBarraProd);
            select = ps.executeQuery();            
        }catch(SQLException | HeadlessException e){            
            JOptionPane.showMessageDialog(null, "Ocurrió un error, contacte al administrador\n"+e.toString(), "Error", 2);        
        }    
    }

    @Override
    public void update(Inventario inventario, int id) {
          Conexion miConexion = new Conexion();
        try{            
            PreparedStatement ps;            
            miConexion.getEnlace().setAutoCommit(false);
            ps = miConexion.getEnlace().prepareStatement(SQLU);
            ps.setString(1, inventario.getDescripcionProd());
            ps.setString(2, inventario.getCodigoBarraProd());
            ps.setString(3, inventario.getNombreMar());
            ps.setString(4, inventario.getNombreTipoPro());
            ps.setBoolean(5, inventario.isDevolucionProd());
            ps.setFloat(6, inventario.getPrecioProd());
            ps.setFloat(7, inventario.getPrecioMayoristaProd());
            ps.setFloat(8, inventario.getDescuentoProd());
            ps.setInt(9, id);                     
            ps.executeUpdate();
            miConexion.getEnlace().commit();
            JOptionPane.showMessageDialog(null, "Registro actualizado exitosamente", "Información", 1);
        }catch(SQLException | HeadlessException e){
            try {
                miConexion.getEnlace().rollback();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
            JOptionPane.showMessageDialog(null, "Ocurrió un error, contacte al administrador\n"+e.toString(), "Error", 2);
        }finally{
            try {
                miConexion.close();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void delete(String dato) {
        JOptionPane.showMessageDialog(null, "Error", "No está permitido eliminar productos", 0);
        Conexion miConexion = new Conexion();
        try{
            int id = Integer.parseInt(dato);
            PreparedStatement ps;            
            miConexion.getEnlace().setAutoCommit(false);
            ps = miConexion.getEnlace().prepareStatement(SQLD);
            ps.setInt(1, id);            
            ps.executeUpdate();
            miConexion.getEnlace().commit();
            JOptionPane.showMessageDialog(null, "Registro eliminado exitosamente", "Información", 1);
        }catch(SQLException | HeadlessException e){
            try {
                miConexion.getEnlace().rollback();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
            JOptionPane.showMessageDialog(null, "Ocurrió un error, contacte al administrador\n"+e.toString(), "Error", 2);
        }finally{
            try {
                miConexion.close();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public ResultSet getSelect() {
        return select;
    }    

    @Override
    public void activarInventario(List<Inventario> inventario) {
        Conexion miConexion = new Conexion();
        try{            
            PreparedStatement ps;            
            miConexion.getEnlace().setAutoCommit(false);            
            //----------Activando productos seleccionados----------------//
            for (Inventario lista : inventario) {
                ps = miConexion.getEnlace().prepareStatement(SQLU_ACTIVAR_DESACTIVAR);
                ps.setString(1, lista.getEstadoProd());            
                ps.setInt(2, lista.getIdProd());                             
                ps.executeUpdate();
            }
            //-----------------------------------------------------------//
            miConexion.getEnlace().commit();
            JOptionPane.showMessageDialog(null, "Productos activados exitosamente", "Información", 1);
        }catch(SQLException | HeadlessException e){
            try {
                miConexion.getEnlace().rollback();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
            JOptionPane.showMessageDialog(null, "Ocurrió un error, contacte al administrador\n"+e.toString(), "Error", 2);
        }finally{
            try {
                miConexion.close();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void desactivarInventario(List<Inventario> inventario) {
        Conexion miConexion = new Conexion();
        try{            
            PreparedStatement ps;            
            miConexion.getEnlace().setAutoCommit(false);            
            //----------Activando productos seleccionados----------------//
            for (Inventario lista : inventario) {
                ps = miConexion.getEnlace().prepareStatement(SQLU_ACTIVAR_DESACTIVAR);
                ps.setString(1, lista.getEstadoProd());            
                ps.setInt(2, lista.getIdProd());                     
                ps.executeUpdate();
            }
            //-----------------------------------------------------------//
            miConexion.getEnlace().commit();
            JOptionPane.showMessageDialog(null, "Productos desactivados exitosamente", "Información", 1);
        }catch(SQLException | HeadlessException e){
            try {
                miConexion.getEnlace().rollback();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
            JOptionPane.showMessageDialog(null, "Ocurrió un error, contacte al administrador\n"+e.toString(), "Error", 2);
        }finally{
            try {
                miConexion.close();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void descuentoInventario(List<Inventario> inventario) {
        Conexion miConexion = new Conexion();
        try{      
            float descuento = 0;
            PreparedStatement ps;            
            miConexion.getEnlace().setAutoCommit(false);            
            //----------Activando productos seleccionados----------------//
            for (Inventario lista : inventario) {
                ps = miConexion.getEnlace().prepareStatement(SQLU_CAMBIAR_DESCUENTO);
                ps.setFloat(1, lista.getDescuentoProd());            
                ps.setInt(2, lista.getIdProd());                     
                ps.executeUpdate();
                descuento = lista.getDescuentoProd();
            }
            //-----------------------------------------------------------//
            miConexion.getEnlace().commit();
            JOptionPane.showMessageDialog(null, "Descuento de "+descuento+"% aplicado exitosamente", "Información", 1);
        }catch(SQLException | HeadlessException e){
            try {
                miConexion.getEnlace().rollback();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
            JOptionPane.showMessageDialog(null, "Ocurrió un error, contacte al administrador\n"+e.toString(), "Error", 2);
        }finally{
            try {
                miConexion.close();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
