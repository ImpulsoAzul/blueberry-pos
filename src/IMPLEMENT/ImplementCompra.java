/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IMPLEMENT;

import INTERFACES.InterfaceCompra;
import MODELO.Compra;
import MODELO.DetalleCompra;
import java.awt.HeadlessException;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Hendrick
 */
public class ImplementCompra implements InterfaceCompra, Serializable{
    private final String SQLC = "insert into compra (nitProv, serieFac, "
            + "numeroFac, fechaComp, totalComp, fechaRegistroComp, tipoComp, "
            + "estadoComp) values (?,?,?,?,?,?,?,?) ";
    private final String SQLCD = "insert into detalleCompra (idComp, idProd, "
            + "cantidadProd, subtotalDetalleComp) values (?,?,?,?)";
    private final String SQLRIDINV = "select existenciaProd from inventario where idProd = ?";
    private final String SQLUINV = "update inventario set existenciaProd=? "
            +"where idProd = ?";    
    private final String SQLRLAST_INSERT_ID = "select LAST_INSERT_ID() as last_id from compra";    
    private final String SQLR = "select * from compra";
    private final String SQLR_IDPROV = "select * from compra where nitProv like ?";
    
    private ResultSet select;

    public ImplementCompra() {
    }
       
    
    @Override
    public void insert(Compra compra, List<DetalleCompra> detalleCompra) {
           Conexion miConexion = new Conexion();
        try{            
            PreparedStatement ps;            
            miConexion.getEnlace().setAutoCommit(false);
            ps = miConexion.getEnlace().prepareStatement(SQLC);
            //insertando el encabezado de la compra
            ps.setString(1, compra.getNitProv());
            ps.setString(2, compra.getSerieFac());
            ps.setInt(3, compra.getNumeroFac());
            ps.setString(4, compra.getFechaComp());
            ps.setFloat(5, compra.getTotalComp());
            ps.setString(6, compra.getFechaRegistroComp());
            ps.setString(7, compra.getTipoComp());
            ps.setString(8, compra.getEstadoComp());
            ps.executeUpdate();
            
            //------------obtiendo el último autoincrement--------------//            
            PreparedStatement Mips = miConexion.getEnlace().prepareStatement(SQLRLAST_INSERT_ID); 
            ResultSet miRs = Mips.executeQuery();
            int lastid = 0;
            while(miRs.next()){
                lastid = Integer.parseInt(miRs.getString("last_id"));    
            }
            
            
            
            //------------obtiendo el último autoincrement--------------//
            //insertando los detalles de la compra
            for (DetalleCompra detalle : detalleCompra) {
                ps = miConexion.getEnlace().prepareStatement(SQLCD);    
                ps.setInt(1, lastid);
                ps.setInt(2, detalle.getidProd());
                ps.setInt(3, detalle.getCantidadProd());
                ps.setFloat(4, detalle.getSubtotalDetalleComp());
                ps.executeUpdate();                
                //------actualizando la existencia del inventario------//
                PreparedStatement psin;
                psin = miConexion.getEnlace().prepareStatement(SQLRIDINV);
                psin.setInt(1, detalle.getidProd());
                ResultSet tmp;
                tmp = psin.executeQuery();
                int existencia = 0;
                while(tmp.next()){
                    existencia = tmp.getInt("existenciaProd");
                }
                existencia+=detalle.getCantidadProd();
                PreparedStatement psuinv;
                psuinv = miConexion.getEnlace().prepareStatement(SQLUINV);
                psuinv.setInt(1, existencia);
                psuinv.setInt(2, detalle.getidProd());
                psuinv.executeUpdate();
                //------actualizando la existencia del inventario------//
            }                                           
            miConexion.getEnlace().commit();
            JOptionPane.showMessageDialog(null, "El ingreso se ha completado exitosamente", "Información", 1);
        }catch(SQLException | HeadlessException e){
            try {
                miConexion.getEnlace().rollback();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
            JOptionPane.showMessageDialog(null, "Ocurrió un error, contacte al administrador\n"+e.toString(), "Error", 2);
        }finally{
            try {
                miConexion.close();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
        } 
    }

    @Override
    public void select() {
        Conexion miConexion = new Conexion();
        try {
            PreparedStatement ps;            
            miConexion.getEnlace().setAutoCommit(false);
            ps = miConexion.getEnlace().prepareStatement(SQLR);
            select = ps.executeQuery();
        } catch (Exception e) {
            try {
                miConexion.getEnlace().rollback();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void selectId(String codigo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(Compra compra, String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(String dato) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public ResultSet getSelect() {
        return select;
    }

    @Override
    public void selectIdProv(String id) {
          Conexion miConexion = new Conexion();
        try {
            PreparedStatement ps;            
            miConexion.getEnlace().setAutoCommit(false);
            ps = miConexion.getEnlace().prepareStatement(SQLR_IDPROV);
            ps.setString(1, '%'+id+'%');
            select = ps.executeQuery();
        } catch (Exception e) {
            try {
                miConexion.getEnlace().rollback();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    
    
}
