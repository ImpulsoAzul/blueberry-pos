/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IMPLEMENT;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author Hendrick
 */
public class ConexionSingleton {
    private static final String DRIVER = "com.mysql.jdbc.Driver";
    private static final String URL = "jdbc:mysql://localhost:3306/blueberry-pos-db";
    private static final String USUARIO = "root";
    private static final String PASSWORD = "root";
    private static ConexionSingleton conexion;
    private Connection enlace;

    private ConexionSingleton() {
        try {
            Connection c;
            Class.forName(DRIVER);
            enlace = DriverManager.getConnection(URL, USUARIO, PASSWORD);            
        } catch (ClassNotFoundException | SQLException e) {
            JOptionPane.showMessageDialog(null, e.toString(), "Información", 0);
        }
    }
 
    public Connection getEnlace() {
        return enlace;
    }
    
    public void close() throws SQLException{
        enlace.close();
    }
    
}
