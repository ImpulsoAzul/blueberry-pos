/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IMPLEMENT;

import INTERFACES.InterfaceResolucion;
import MODELO.Resolucion;
import java.awt.HeadlessException;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Hendrick
 */
public class ImplementResolucion implements InterfaceResolucion, Serializable{
    private final String SQLC_INSERT = "INSERT INTO resolucion (nombreDoc, "
            + "idEmisor, numeroResolucion, fechaResolucion, "
            + "serieResolucion, delResolucion, alResolucion, estadoResolucion) "
            + "VALUES (?,?,?,?,?,?,?,?)";
    private final String SQLR_SELECT = "select * from resolucion";
    private final String SQLR_SELECT_LAST_DOC = "SELECT * FROM resolucion "
            + "where nombreDoc = ? ORDER BY idResolucion DESC LIMIT 1";
    private ResultSet select;
    
    
    public ImplementResolucion() {
    }       
    
    @Override
    public void insert(Resolucion resolucion) {
         Conexion miConexion = new Conexion();
        try{            
            PreparedStatement ps;            
            miConexion.getEnlace().setAutoCommit(false);
            ps = miConexion.getEnlace().prepareStatement(SQLC_INSERT);
            ps.setString(1, resolucion.getNombreDoc().trim());
            ps.setString(2, resolucion.getIdEmisor().trim());
            ps.setString(3, resolucion.getNumeroResolucion().trim());
            ps.setString(4, resolucion.getFechaResolucion().trim());
            ps.setString(5, resolucion.getSerieResolucion().trim());
            ps.setInt(6, resolucion.getDelResolucion());
            ps.setInt(7, resolucion.getAlResolucion());
            ps.setString(8, resolucion.getEstadoResolucion().trim());
            ps.executeUpdate();
            miConexion.getEnlace().commit();
            JOptionPane.showMessageDialog(null, "Registro almacenado exitosamente", "Información", 1);
        }catch(SQLException | HeadlessException e){
            try {
                miConexion.getEnlace().rollback();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
            JOptionPane.showMessageDialog(null, "Ocurrió un error, contacte al administrador\n"+e.toString(), "Error", 2);
        }finally{
            try {
                miConexion.close();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void select() {
        Conexion miConexion = new Conexion();
        try{
            PreparedStatement ps;            
            ps = miConexion.getEnlace().prepareStatement(SQLR_SELECT);
            select = ps.executeQuery();            
        }catch(SQLException | HeadlessException e){
            try {
                miConexion.getEnlace().rollback();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
            JOptionPane.showMessageDialog(null, "Ocurrió un error, contacte al administrador\n"+e.toString(), "Error", 2);        
        }
    }
    

    @Override
    public void selectId(int codigo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public ResultSet getSelect() {
        return select;
    }

    @Override
    public void selectDocumento(String documento) {
        Conexion miConexion = new Conexion();
        try{
            PreparedStatement ps;            
            ps = miConexion.getEnlace().prepareStatement(SQLR_SELECT_LAST_DOC);
            ps.setString(1, documento);
            select = ps.executeQuery();            
        }catch(SQLException | HeadlessException e){
            try {
                miConexion.getEnlace().rollback();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
            JOptionPane.showMessageDialog(null, "Ocurrió un error, contacte al administrador\n"+e.toString(), "Error", 2);        
        }
    }
   
    
}
