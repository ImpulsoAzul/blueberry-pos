/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IMPLEMENT;

import INTERFACES.InterfaceMarca;
import MODELO.Marca;
import java.awt.HeadlessException;
import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Hendrick
 */
public class ImplementMarca implements InterfaceMarca, Serializable {

    private final String SQLC = "insert into marca (nombreMar) "
            + "values (?) ";
    private final String SQLR = "select * from marca";
    private final String SQLRID = "select * from marca where nombreMar = ?";
    private final String SQLU = "update marca set nombreMar= ? "
            + "where nombreMar= ?";
    private final String SQLD = "delete from marca where nombreMar = ?";
    private ResultSet select;
    private List<Marca> listaMarcas;
    private final String findAll = "select * from findAllMarca";

    public ImplementMarca() {
    }

    @Override
    public void insert(Marca marca) {
        Conexion miConexion = new Conexion();
        try {
            miConexion.getEnlace().setAutoCommit(false);
            CallableStatement cStmt = miConexion.getEnlace().prepareCall("{call insertMarca(?)}");
            cStmt.setString(1, marca.getNombremarca());
            cStmt.execute();
            miConexion.getEnlace().commit();
            JOptionPane.showMessageDialog(null, "Registro almacenado exitosamente", "Información", 1);
        } catch (SQLException | HeadlessException e) {
            try {
                miConexion.getEnlace().rollback();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
            JOptionPane.showMessageDialog(null, "Ocurrió un error, contacte al administrador\n" + e.toString(), "Error", 2);
        } finally {
            try {
                miConexion.close();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public List<Marca> findAllMarca() {
        Conexion miConexion = new Conexion();
        listaMarcas = new ArrayList<>();
        Marca marca;
        try {
            PreparedStatement ps;
            ps = miConexion.getEnlace().prepareStatement(this.findAll);
            select = ps.executeQuery();
            while(select.next()){
                marca = new Marca();
                marca.setNombremarca(select.getString("nombremarca"));                                
                marca.setEstadomarca(select.getBoolean("estadomarca"));
                listaMarcas.add(marca);
            }
        } catch (SQLException | HeadlessException e) {
            try {
                miConexion.getEnlace().rollback();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
            JOptionPane.showMessageDialog(null, "Ocurrió un error, contacte al administrador\n" + e.toString(), "Error", 2);
        } finally {
            try {
                miConexion.close();
                select.close();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementMarca.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return listaMarcas;
    }

    @Override
    public void selectId(String codigo) {
        Conexion miConexion = new Conexion();
        try {
            PreparedStatement ps;
            ps = miConexion.getEnlace().prepareStatement(SQLRID);
            ps.setString(1, codigo);
            select = ps.executeQuery();
        } catch (SQLException | HeadlessException e) {
            try {
                miConexion.getEnlace().rollback();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
            JOptionPane.showMessageDialog(null, "Ocurrió un error, contacte al administrador\n" + e.toString(), "Error", 2);
        }
    }

    @Override
    public void update(Marca marca, String id) {
        Conexion miConexion = new Conexion();
        try {
            PreparedStatement ps;
            miConexion.getEnlace().setAutoCommit(false);
            ps = miConexion.getEnlace().prepareStatement(SQLU);
            ps.setString(1, marca.getNombremarca());
            ps.setString(2, id);
            ps.executeUpdate();
            miConexion.getEnlace().commit();
            JOptionPane.showMessageDialog(null, "Registro actualizado exitosamente", "Información", 1);
        } catch (SQLException | HeadlessException e) {
            try {
                miConexion.getEnlace().rollback();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
            JOptionPane.showMessageDialog(null, "Ocurrió un error, contacte al administrador\n" + e.toString(), "Error", 2);
        } finally {
            try {
                miConexion.close();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void delete(String dato) {
        Conexion miConexion = new Conexion();
        try {
            PreparedStatement ps;
            miConexion.getEnlace().setAutoCommit(false);
            ps = miConexion.getEnlace().prepareStatement(SQLD);
            ps.setString(1, dato);
            ps.executeUpdate();
            miConexion.getEnlace().commit();
            JOptionPane.showMessageDialog(null, "Registro eliminado exitosamente", "Información", 1);
        } catch (SQLException | HeadlessException e) {
            try {
                miConexion.getEnlace().rollback();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
            JOptionPane.showMessageDialog(null, "Ocurrió un error, contacte al administrador\n" + e.toString(), "Error", 2);
        } finally {
            try {
                miConexion.close();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public ResultSet getSelect() {
        return select;
    }

}
