/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IMPLEMENT;

import INTERFACES.InterfaceDetalleCompra;
import MODELO.DetalleCompra;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hendrick
 */
public class ImplementDetalleCompra implements InterfaceDetalleCompra, Serializable{
    private final String SQLR_IDCOMPRA = "select * from detallecompra "
            + "INNER JOIN inventario ON detallecompra.idProd = inventario.idProd "
            + "where detallecompra.idComp = ?";
    private ResultSet select;

    public ImplementDetalleCompra() {
    }
        
    
    @Override
    public void insert(DetalleCompra detalleCompra) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void select() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void selectId(int id) {
           Conexion miConexion = new Conexion();
        try {
            PreparedStatement ps;            
            miConexion.getEnlace().setAutoCommit(false);
            ps = miConexion.getEnlace().prepareStatement(SQLR_IDCOMPRA);
            ps.setInt(1, id);
            select = ps.executeQuery();
        } catch (Exception e) {
            try {
                miConexion.getEnlace().rollback();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void update(DetalleCompra detalleCompra, String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(String dato) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public ResultSet getSelect() {
        return select;
    }
    
    
    
}
