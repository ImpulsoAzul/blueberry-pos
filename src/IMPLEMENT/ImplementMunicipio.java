/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IMPLEMENT;

import INTERFACES.InterfaceMunicipio;
import MODELO.Municipio;
import java.awt.HeadlessException;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Hendrick
 */
public class ImplementMunicipio implements InterfaceMunicipio, Serializable{
    private final String SQLR = "select * from municipio";
    private final String SQLRID = "select nombreMuni from municipio where nombreDepto = ?";
    private ResultSet select;
    
    public ImplementMunicipio() {
    }
            
    
    @Override
    public void insert(Municipio municipio) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void select() {
        Conexion miConexion = new Conexion();
        try{
            PreparedStatement ps;            
            ps = miConexion.getEnlace().prepareStatement(SQLR);            
            select = ps.executeQuery();             
        }catch(SQLException | HeadlessException e){
            try {
                miConexion.getEnlace().rollback();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
            JOptionPane.showMessageDialog(null, "Ocurrió un error, contacte al administrador\n"+e.toString(), "Error", 2);        
        }
    }

    @Override
    public void selectId(String codigo) {
           Conexion miConexion = new Conexion();
        try{
            PreparedStatement ps;            
            ps = miConexion.getEnlace().prepareStatement(SQLRID);
            ps.setString(1, codigo);
            select = ps.executeQuery();             
        }catch(SQLException | HeadlessException e){
            try {
                miConexion.getEnlace().rollback();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
            JOptionPane.showMessageDialog(null, "Ocurrió un error, contacte al administrador\n"+e.toString(), "Error", 2);        
        }
    }

    @Override
    public void update(Municipio municipio, String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(String dato) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public ResultSet getSelect(){
        return select;
    }
    
}
