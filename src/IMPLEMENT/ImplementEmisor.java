/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IMPLEMENT;

import INTERFACES.InterfaceEmisor;
import MODELO.Emisor;
import java.awt.HeadlessException;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Hendrick
 */
public class ImplementEmisor implements InterfaceEmisor, Serializable{
    private final String SQLC_INSERT="INSERT INTO emisor(idEmisor, "
            + "nombreComercialEmisor, denominacionSocialEmisor, direccionEmisor, "
            + "representanteEmisor, nombreMuni, numeroResolucionSistema, "
            + "fechaResolucionSistema, fechaCreacionEmisor, fechaExpiraEmisor, "
            + "estadoEmisor) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
    private final String SQLR_SELECT_ALL="select * from emisor";
    private final String SQLR_SELECT_COUNT= "select COUNT(*) as cantidad FROM emisor";
    private final String SQLU_ACTIVATE = "update emisor set "
            + "fechaExpiraEmisor = ?, estadoEmisor = ? where idEmisor = ?";
    private ResultSet select;
    
    
    public ImplementEmisor() {
    }
        

    @Override
    public void insert(Emisor emisor) {
         Conexion miConexion = new Conexion();
        try{            
            PreparedStatement ps;            
            miConexion.getEnlace().setAutoCommit(false);
            ps = miConexion.getEnlace().prepareStatement(SQLC_INSERT);
            ps.setString(1, emisor.getIdEmisor() );
            ps.setString(2, emisor.getNombreComercialEmisor());
            ps.setString(3, emisor.getDenominacionSocialEmisor());
            ps.setString(4, emisor.getDireccionEmisor());
            ps.setString(5, emisor.getRepresentanteEmisor());
            ps.setString(6, emisor.getNombreMuni());
            ps.setString(7, emisor.getNumeroResolucionSistema());
            ps.setString(8, emisor.getFechaResolucionSistema());
            ps.setString(9, emisor.getFechaCreacionEmisor());
            ps.setString(10, emisor.getFechaExpiraEmisor());
            ps.setString(11, emisor.getEstadoEmisor());
            ps.executeUpdate();
            miConexion.getEnlace().commit();
            JOptionPane.showMessageDialog(null, "Registro almacenado exitosamente", "Información", 1);
        }catch(SQLException | HeadlessException e){
            try {
                miConexion.getEnlace().rollback();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
            JOptionPane.showMessageDialog(null, "Ocurrió un error, contacte al administrador\n"+e.toString(), "Error", 2);
        }finally{
            try {
                miConexion.close();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void select() {
        Conexion miConexion = new Conexion();
        try{
            PreparedStatement ps;            
            ps = miConexion.getEnlace().prepareStatement(SQLR_SELECT_ALL);
            select = ps.executeQuery();            
        }catch(SQLException | HeadlessException e){
            try {
                miConexion.getEnlace().rollback();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
            JOptionPane.showMessageDialog(null, "Ocurrió un error, contacte al administrador\n"+e.toString(), "Error", 2);        
        }
    }

    @Override
    public void selectId(String codigo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(Emisor emisor) {
        Conexion miConexion = new Conexion();
        try{            
            PreparedStatement ps;            
            miConexion.getEnlace().setAutoCommit(false);
            ps = miConexion.getEnlace().prepareStatement(SQLU_ACTIVATE);
            ps.setString(1, emisor.getFechaExpiraEmisor());            
            ps.setString(2, emisor.getEstadoEmisor());
            ps.setString(3, emisor.getIdEmisor());
            ps.executeUpdate();
            miConexion.getEnlace().commit();
            JOptionPane.showMessageDialog(null, "Emisor activado exitosamente", "Información", 1);
        }catch(SQLException | HeadlessException e){
            try {
                miConexion.getEnlace().rollback();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
            JOptionPane.showMessageDialog(null, "Ocurrió un error, contacte al administrador\n"+e.toString(), "Error", 2);
        }finally{
            try {
                miConexion.close();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    @Override
    public void delete(String dato) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void selectCount() {
           Conexion miConexion = new Conexion();
        try{
            PreparedStatement ps;            
            ps = miConexion.getEnlace().prepareStatement(SQLR_SELECT_COUNT);
            select = ps.executeQuery();            
        }catch(SQLException | HeadlessException e){
            try {
                miConexion.getEnlace().rollback();
            } catch (SQLException ex) {
                Logger.getLogger(ImplementDocumento.class.getName()).log(Level.SEVERE, null, ex);
            }
            JOptionPane.showMessageDialog(null, "Ocurrió un error, contacte al administrador\n"+e.toString(), "Error", 2);        
        }
    }
    
    public ResultSet getSelect() {
        return select;
    }

  
    
    
    
}
