/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package INTERFACES;

import MODELO.Municipio;

/**
 *
 * @author Hendrick
 */
public interface InterfaceMunicipio {
    public void insert(Municipio municipio);
    public void select();
    public void selectId(String codigo);
    public void update(Municipio municipio, String id);
    public void delete(String dato);
}
