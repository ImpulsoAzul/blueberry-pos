/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package INTERFACES;

import MODELO.Telefono;

/**
 *
 * @author Hendrick
 */
public interface InterfaceTelefono {
    public void insert(Telefono telefono);
    public void select();
    public void selectId(String codigo);
    public void update(Telefono telefono, String id);
    public void delete(String dato);    
}
