/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package INTERFACES;

import MODELO.Emisor;

/**
 *
 * @author Hendrick
 */
public interface InterfaceEmisor {
    public void insert(Emisor emisor);
    public void select();
    public void selectCount();
    public void selectId(String codigo);
    public void update(Emisor emisor);
    public void delete(String dato);  
}
