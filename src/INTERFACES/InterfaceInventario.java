/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package INTERFACES;

import MODELO.Inventario;
import java.util.List;

/**
 *
 * @author Hendrick
 */
public interface InterfaceInventario {
    public void insert(Inventario inventario);
    public void select();
    public void selectId(int idProd);
    public void selectLike(String descripcionProd);
    public void selectAvanzado(String descripcionProd, String codigoBarraProd, 
            float precioProdMin, float precioProdMax, int existenciaProdMin, 
            int existenciaProdMax, String estadoProd);
    public void selectBarra(String codigoBarraProd);
    public void activarInventario(List<Inventario> inventario);
    public void desactivarInventario(List<Inventario> inventario);
    public void descuentoInventario(List<Inventario> inventario);
    public void update(Inventario inventario, int id);
    public void delete(String dato);
}
