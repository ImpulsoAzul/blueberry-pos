/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package INTERFACES;

import MODELO.DetalleCompra;

/**
 *
 * @author Hendrick
 */
public interface InterfaceDetalleCompra {
    public void insert(DetalleCompra detalleCompra);    
    public void select();
    public void selectId(int id);
    public void update(DetalleCompra detalleCompra, String id);
    public void delete(String dato);
}
