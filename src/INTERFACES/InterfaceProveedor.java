/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package INTERFACES;

import MODELO.Email;
import MODELO.Proveedor;
import MODELO.Telefono;
import java.util.List;

/**
 *
 * @author Hendrick
 */
public interface InterfaceProveedor {
    public void insert(Proveedor proveedor, List<Telefono> telefonos,List<Email> correos);    
    public void select();
    public void selectId(String codigo);
    public void update(Proveedor proveedor, String id, List<Telefono> telefonos,List<Email> correos);
    public void delete(String dato);
}
