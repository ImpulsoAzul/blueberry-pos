/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package INTERFACES;

import MODELO.Resolucion;

/**
 *
 * @author Hendrick
 */
public interface InterfaceResolucion {
    public void insert(Resolucion resolucion);
    public void select();
    public void selectDocumento(String documento);
    public void selectId(int codigo);    
}
