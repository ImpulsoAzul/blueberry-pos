/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package INTERFACES;

import MODELO.Compra;
import MODELO.DetalleCompra;
import java.util.List;

/**
 *
 * @author Hendrick
 */
public interface InterfaceCompra {
    public void insert(Compra compra, List<DetalleCompra> detalleCompra);    
    public void select();
    public void selectId(String codigo);
    public void selectIdProv(String id);
    public void update(Compra compra, String id);
    public void delete(String dato);
}
