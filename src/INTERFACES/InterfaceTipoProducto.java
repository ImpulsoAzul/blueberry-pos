/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package INTERFACES;

import MODELO.TipoProducto;

/**
 *
 * @author Hendrick
 */
public interface InterfaceTipoProducto {
    public void insert(TipoProducto tipoProducto);
    public void select();
    public void selectId(String codigo);
    public void update(TipoProducto tipoProducto, String id);
    public void delete(String dato);
}
