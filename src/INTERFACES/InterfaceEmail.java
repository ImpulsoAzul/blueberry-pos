/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package INTERFACES;

import MODELO.Email;

/**
 *
 * @author Hendrick
 */
public interface InterfaceEmail {
    public void insert(Email email);
    public void select();
    public void selectId(String codigo);
    public void update(Email email, String id);
    public void delete(String dato);    
}
