/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package INTERFACES;

import MODELO.Departamento;

/**
 *
 * @author Hendrick
 */
public interface InterfaceDepartamento {
    public void insert(Departamento departamento);
    public void select();
    public void selectId(String codigo);
    public void update(Departamento departamento, String id);
    public void delete(String dato);
}
