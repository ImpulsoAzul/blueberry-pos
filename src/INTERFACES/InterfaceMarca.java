/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package INTERFACES;

import MODELO.Marca;
import java.util.List;

/**
 *
 * @author Hendrick
 */
public interface InterfaceMarca {
    public void insert(Marca marca);
    public List<Marca> findAllMarca();
    public void selectId(String codigo);
    public void update(Marca marca, String id);
    public void delete(String dato);
}
