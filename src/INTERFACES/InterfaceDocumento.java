/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package INTERFACES;

import MODELO.Documento;

/**
 *
 * @author Hendrick
 */
public interface InterfaceDocumento {
    public void insert(Documento documento);
    public void select();
    public void selectId(String codigo);
    public void update(Documento documento, String id);
    public void delete(String dato);    
}
