/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LOGICA;

import IMPLEMENT.ImplementMarca;
import MODELO.Marca;
import javax.swing.JOptionPane;

/**
 *
 * @author Hendrick
 */
public class LogicMarca {

    public LogicMarca() {
    }
    
    public void nuevaMarca(Marca miMarca, String id, String flag){
        try {
            ImplementMarca tran = new ImplementMarca();
            if (miMarca.getNombremarca().trim().length() == 0) throw new Exception("El campo nombre de la marca no puede ser vacio.");
            if (flag.equals("+")){
                tran.insert(miMarca);                                              
            }else{
                tran.update(miMarca, id);
            }                        
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.toString(), "Error", 0);
        }
    }      
}
