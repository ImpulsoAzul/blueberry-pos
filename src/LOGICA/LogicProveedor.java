/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LOGICA;

import IMPLEMENT.ImplementProveedor;
import MODELO.Email;
import MODELO.Proveedor;
import MODELO.Telefono;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Hendrick
 */
public class LogicProveedor {

    public LogicProveedor() {
    }
    
    public void nuevoProveedor(Proveedor miProveedor, 
            List<Telefono> misTelefonos, List<Email> misCorreos, 
            String flag, String id){                       
        try {
            if(miProveedor.getNitProv().trim().length() == 0) throw new Exception("El nit no puede ser vacio");                        
            ImplementProveedor tran = new ImplementProveedor();
            if(flag.equals("Guardar")){
                tran.insert(miProveedor, misTelefonos, misCorreos);
            }else{
                tran.update(miProveedor, id, misTelefonos, misCorreos);
            }    
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.toString(), "Error", 0);
        }
        
    }
    
}
