/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LOGICA;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


/**
 *
 * @author Hendrick
 */
public class LogicFecha {  
    
    
    public static String Obtener_Fecha(String datePicker){                     
        String meses[]= {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};             
        String splits[]=datePicker.split(" ");             
        int mes = 0;             
        for(int i=0;i<=meses.length-1;i++)                     
            if(splits[1].equals(meses[i]))                             
                mes=i+1;            
                String month = "" + mes;             
                if(month.length()==1)                     
                    month="0"+mes;             
                else                     
                    month=""+mes;             
                return splits[splits.length-1]+"/"+month+"/"+splits[2];                   
    }
    
    public static boolean compararFecha(String fechaCompra){
        boolean resultado = false;
        SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
        try {
            Date fechaC = format.parse(fechaCompra);
            Calendar c = Calendar.getInstance();
            String dia = Integer.toString(c.get(Calendar.DATE));
            String mes = Integer.toString(c.get(Calendar.MONTH));
            String anio = Integer.toString(c.get(Calendar.YEAR));            
            String fechaSistema = anio+"/"+mes+"/"+dia;
            Date fechaS = format.parse(fechaSistema);
            if (fechaC.before(fechaS)){
                resultado = true;
            }else{
                resultado = !fechaS.before(fechaC);
            }
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return resultado;        
    }
}
