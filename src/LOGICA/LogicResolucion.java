/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LOGICA;

import IMPLEMENT.ImplementResolucion;
import MODELO.Resolucion;
import java.sql.ResultSet;

/**
 *
 * @author Hendrick
 */
public class LogicResolucion {
    
    public void nuevaResolucion(Resolucion resolucion) throws Throwable{
        try {
            ImplementResolucion implementResolucion = new ImplementResolucion();
            implementResolucion.selectDocumento(resolucion.getNombreDoc());
            ResultSet rs = implementResolucion.getSelect();
            String estadoActual = null;
            while(rs.next()){
                estadoActual = rs.getString("estadoResolucion");
            }
            if(estadoActual==null){
                if(resolucion.getDelResolucion() > resolucion.getAlResolucion()) 
                    throw new Throwable("Revise la resolución emitida por la SAT.\n"
                        + "El número 'del' de la resolución no puede ser mayor que el 'al'.");
                implementResolucion.insert(resolucion);
                
            }else{    
                if (estadoActual.equals("A")){
                throw new Throwable("El documento seleccionado no ha alcanzado el límite establecido.\n"
                        + "Podrá registrar una nueva resolución para ese documento luego que alcance su límite.");
                }else{
                    if(resolucion.getDelResolucion() > resolucion.getAlResolucion()){
                    throw new Throwable("Revise la resolución emitida por la SAT.\n"
                        + "El número 'del' de la resolución no puede ser mayor que el 'al'.");
                    }else{
                    implementResolucion.insert(resolucion);
                }                
            }            
            }                        
        } catch (Exception e) {
        }
    }
    
}
