/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LOGICA;

import IMPLEMENT.ImplementCompra;
import MODELO.Compra;
import MODELO.DetalleCompra;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Hendrick
 */
public class LogicCompra {

    public LogicCompra() {
    }
    
    public void nuevaCompra(Compra miCompra, List<DetalleCompra> miDetalle, float total, float cuadre) throws Throwable{                         
        try {
            if(total==0) throw new Throwable("El total de la compra no puede ser 0");
            if(total!=cuadre ) throw new Throwable("El detalle no cuadra con el total definido");
            if(miDetalle.isEmpty()) throw new Throwable("Es necesario que la compra tenga detalle");
            ImplementCompra Miscompras = new ImplementCompra();
            Miscompras.insert(miCompra, miDetalle);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.toString(), "Error", 0);
        }
        
        
    }
    
    public void detallarCompra(){
        
    }
    
}
