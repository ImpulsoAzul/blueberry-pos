/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LOGICA;

import IMPLEMENT.ImplementTipoProducto;
import MODELO.TipoProducto;
import javax.swing.JOptionPane;

/**
 *
 * @author Hendrick
 */
public class LogicTipoProducto {

    public LogicTipoProducto() {
    }
    
    public void nuevoTipo(TipoProducto miTipo, String id, String flag){
        try {
            ImplementTipoProducto tran = new ImplementTipoProducto();
            if (miTipo.getNombreTipoPro().trim().length() == 0) throw new Exception("El campo 'Tipo de Producto' no puede ser vacio.");
            if (flag.equals("Guardar")){
                tran.insert(miTipo);                                              
            }else{
                tran.update(miTipo, id);
            }                        
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.toString(), "Error", 0);
        }
    } 
}
