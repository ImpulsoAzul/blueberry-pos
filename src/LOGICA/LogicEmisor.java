/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LOGICA;

import IMPLEMENT.ImplementEmisor;
import MODELO.Emisor;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author Hendrick
 */
public class LogicEmisor {

    public LogicEmisor() {
    }
    
    public void nuevoEmisor(Emisor emisor) throws Throwable{
        try {
            ImplementEmisor implementEmisor = new ImplementEmisor();
            implementEmisor.selectCount();
            ResultSet rs = implementEmisor.getSelect();
            int cantidad = 0;
            while(rs.next()){
                cantidad = Integer.parseInt(rs.getString("cantidad"));
            }
            if(cantidad>0){
                throw new Throwable("No se puede registrar más de un emisor");
            }else{
                implementEmisor.insert(emisor);
            }
            
        } catch (SQLException | NumberFormatException e) {
            JOptionPane.showMessageDialog(null, e.toString(), "Error", 0);
        }
    }
    
    public void activarEmisor(Emisor emisor){
        try {
            ImplementEmisor implementEmisor = new ImplementEmisor();
            implementEmisor.update(emisor);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.toString(), "Error", 0);
        }
    }
    
}
