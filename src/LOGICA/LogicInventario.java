/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LOGICA;

import IMPLEMENT.ImplementInventario;
import MODELO.Inventario;
import java.sql.ResultSet;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Hendrick
 */
public class LogicInventario {
    private ResultSet rs;

    public LogicInventario() {
    }
    
    public void nuevoInventario(Inventario miInventario, int id, String flag){
        try {
            if (miInventario.getDescripcionProd().trim().length() == 250 ) throw new Exception("La descripion debe ser menor que 250 caracteres");
            if (miInventario.getPrecioProd() < miInventario.getPrecioMayoristaProd() ) throw new Exception("El precio mayorista no devería ser menor que el precio normal");
            ImplementInventario tran = new ImplementInventario();
            if(flag.equals("Guardar")){
                tran.insert(miInventario);
            }else{
                tran.update(miInventario, id);
            }            
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.toString(), "Error", 1);
        }
    }
    
    public ResultSet busquedaAvanzada(String descripcionProd, String codigoBarraProd, 
            String precioProdMin, String precioProdMax,
            String existenciaProdMin, String existenciaProdMax, boolean estadoProd){
        float precioMin;
        float precioMax;
        int existenciaMin;
        int existenciaMax;
        String estado;
        try{
            String descripcion = descripcionProd.trim();
            String codigoBarra = codigoBarraProd.trim();
            if(precioProdMin.trim().length()>0){
                precioMin = Float.parseFloat(precioProdMin.trim());
            }else{
                precioMin = 0;
            }
            if(precioProdMax.trim().length()>0){
                precioMax = Float.parseFloat(precioProdMax.trim());
            }else{
                precioMax = 100000000;
            }
            if(existenciaProdMin.trim().length()>0){
                existenciaMin = Integer.parseInt(existenciaProdMin.trim());
            }else{
                existenciaMin = 0;
            }
            if(existenciaProdMax.trim().length()>0){
                existenciaMax = Integer.parseInt(existenciaProdMax.trim());
            }else{
                existenciaMax = 100000000;
            }   
            if(estadoProd){
                estado = "A";                                      
            }else{
                estado = "D";
            }
             
            ImplementInventario miBusqueda = new ImplementInventario();                
            miBusqueda.selectAvanzado(descripcion, codigoBarra, 
                    precioMin, precioMax, existenciaMin, existenciaMax,estado);
            rs = miBusqueda.getSelect();
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Datos ingresados equivocadamente: \n"+e.toString(), "Error", 0);
        }
    return rs;    
    }        

    
    public void cambiarEstadoInventario(List<Inventario> miInventario, String flag){
        try {
            ImplementInventario implementInventario = new ImplementInventario();
            if (flag.equals("A")){
                implementInventario.activarInventario(miInventario);    
            }else{
                implementInventario.desactivarInventario(miInventario);    
            }                        
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.toString(), "Error", 0);
        }
    }
    
    public void cambiarDescuentoInventario(List<Inventario> miInventario, String descuento) throws Throwable{
        try {
            float miDescuento = Float.parseFloat(descuento);
            if(miDescuento > 100)throw new Throwable("El descuento no puede ser mayor que el 100%");
            miInventario.stream().forEach((inventario) -> {
                inventario.setDescuentoProd(miDescuento);
            });
            ImplementInventario implementInventario = new ImplementInventario();
            implementInventario.descuentoInventario(miInventario);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "El descuento ingresado no es un valor aceptable", "Error", 0);
        }
    }
}
