/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package VISTA;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.JOptionPane;
import java.util.Properties;

/**
 *
 * @author Hcalderón
 */
public class FrmLogin extends javax.swing.JFrame {
    public static String urldb;
    /**
     * Creates new form FrmLogin
     */
    public FrmLogin() {
        initComponents();
        lb_ubicacion.setVisible(false);
        jtf_ubicacion.setVisible(false);
        
        //Escaneando unidades en busca del archivo de configuración siscop-pos-dll
       
        String[] letra = new String[20];
        letra[0] = "A";
        letra[1] = "B";
        letra[2] = "C";
        letra[3] = "D";
        letra[4] = "E";
        letra[5] = "F";
        letra[6] = "G";
        letra[7] = "H";
        letra[8] = "I";
        letra[9] = "J";
        letra[10] = "K";
        letra[11] = "L";
        letra[12] = "M";
        letra[13] = "N";
        letra[14] = "Ñ";
        letra[15] = "O";
        letra[16] = "P";
        letra[17] = "Q";
        letra[18] = "R";
        letra[19] = "S";
        
        File[] archivosconf = new File[20];
        boolean[] existe = new boolean[20];
        short i = 0;
        short indice = 0;
        do{
           archivosconf[i] = new File(letra[i]+":\\siscop-pos.dll");
           existe[i] = archivosconf[i].exists();
           if (existe[i] == true){
               indice = i;
           }
           i+= 1;                                
        }while(i<20); 
        
         if (existe[indice] == true){ //Si existe es SuperAdmin
                try {
                String linea;                  
                short x = 1;                
                BufferedReader flujoEntrada;                               
                flujoEntrada = new BufferedReader(new FileReader(archivosconf[indice]));
                while ((linea = flujoEntrada.readLine()) != null) {                    
                    switch (x){
                        case 1:
                            jtflogin_user.setText(linea);
                        case 2:
                            jpfclave_user.setText(linea);          
                    }                    
                    x+= 1;
                }
                lb_ubicacion.setVisible(true);
                jtf_ubicacion.setVisible(true);
                }catch(IOException ex){
                    System.out.println(ex.toString());
                }
           }else{   // de lo contrario es usuario normal
                    //Obteniendo datos de los archivos conf. sopcil.dll, sopmret.dll
                File Archivolic = new File("C:\\windows\\system32\\sopcil.dll");
                boolean existe1 = Archivolic.exists();
        
                File Archivoterm = new File("C:\\windows\\system32\\sopmret.dll");
                boolean existe2 = Archivoterm.exists();
        
                if (existe1 == false || existe2 == false){
                    JOptionPane.showMessageDialog(rootPane, "Se ha perdido la "
                            + "configuración del sistema que fué establecida\n"
                            + "por el SuperAdministrador.\nPóngase con contacto"
                            + " con el Proveedor del Sistema...", 
                            "Ocurrió un error", JOptionPane.ERROR_MESSAGE);
                      JOptionPane.showMessageDialog(rootPane, "El sistema se"
                              + " apagará en éste momento...", 
                            "Ocurrió un error", JOptionPane.ERROR_MESSAGE);
                    System.exit(0); //Si no existen muestra mensaje y cierra
                }else{
                try {
                    String linea;
                    String linea2;  
                    short x = 1;
                    short y = 1;
                    BufferedReader flujoEntrada;
                    BufferedReader flujoEntrada2;
                
                    flujoEntrada = new BufferedReader(new FileReader(Archivolic));
                    while ((linea = flujoEntrada.readLine()) != null) {                    
                        switch (x){
                            case 1:
                                LbNitEmpresa.setText(linea);
                            case 2:
                                LbNombreEmpresa.setText(linea);
                            case 3:                            
                                LbNoLicencia.setText(linea);
                        }                    
                        x+= 1;
                        }
                    
                    flujoEntrada2 = new BufferedReader(new FileReader(Archivoterm));
                    while ((linea2 = flujoEntrada2.readLine()) != null) {                    
                        switch (y){                        
                            case 2:
                                LbTerminal.setText(linea2);
                            case 3:
                                LbNoResolucion.setText(linea2);
                            case 4:
                                LbFechaResolucion.setText(linea2);
                            case 5:
                                urldb = linea2;
                            }                    
                        y+= 1;
                        }
                    } catch (IOException e) {
                        System.out.println(e.toString());
                    }
                }
            }
}      
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jpfclave_user = new javax.swing.JPasswordField();
        jtflogin_user = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        btIngresar = new javax.swing.JButton();
        btSalir = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel3 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        LbNoLicencia = new javax.swing.JLabel();
        LbNombreEmpresa = new javax.swing.JLabel();
        LbNitEmpresa = new javax.swing.JLabel();
        lb1 = new javax.swing.JLabel();
        lb2 = new javax.swing.JLabel();
        lb3 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        lb4 = new javax.swing.JLabel();
        lb5 = new javax.swing.JLabel();
        lb6 = new javax.swing.JLabel();
        LbTerminal = new javax.swing.JLabel();
        LbNoResolucion = new javax.swing.JLabel();
        LbFechaResolucion = new javax.swing.JLabel();
        lb_ubicacion = new javax.swing.JLabel();
        jtf_ubicacion = new javax.swing.JPasswordField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("INGRESO AL SISTEMA");
        setName("frameLogin"); // NOI18N
        setType(java.awt.Window.Type.UTILITY);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setPreferredSize(new java.awt.Dimension(610, 550));

        jpfclave_user.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jtflogin_user.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Contraseña: ");

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setText("Usuario: ");

        btIngresar.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N
        btIngresar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/IconoSesion.png"))); // NOI18N
        btIngresar.setText("Ingresar");
        btIngresar.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btIngresar.setBorderPainted(false);
        btIngresar.setContentAreaFilled(false);
        btIngresar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btIngresar.setDefaultCapable(false);
        btIngresar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btIngresar.setIconTextGap(-3);
        btIngresar.setPreferredSize(new java.awt.Dimension(80, 80));
        btIngresar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btIngresar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btIngresarActionPerformed(evt);
            }
        });

        btSalir.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N
        btSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/IconoCancelar.png"))); // NOI18N
        btSalir.setText("Salir");
        btSalir.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btSalir.setBorderPainted(false);
        btSalir.setContentAreaFilled(false);
        btSalir.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btSalir.setDefaultCapable(false);
        btSalir.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btSalir.setIconTextGap(-3);
        btSalir.setPreferredSize(new java.awt.Dimension(80, 80));
        btSalir.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSalirActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/FondoSisCop-POS.jpg"))); // NOI18N
        jLabel3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        LbNoLicencia.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        LbNoLicencia.setText("No licencia");

        LbNombreEmpresa.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        LbNombreEmpresa.setText("No empresa");

        LbNitEmpresa.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        LbNitEmpresa.setText("No nit");

        lb1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lb1.setText("Licencia No.");

        lb2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lb2.setText("Acreditada a:");

        lb3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lb3.setText("Nit:");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(222, 222, 222)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lb3)
                    .addComponent(lb1)
                    .addComponent(lb2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(LbNitEmpresa)
                    .addComponent(LbNoLicencia)
                    .addComponent(LbNombreEmpresa))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lb1)
                    .addComponent(LbNoLicencia))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lb3)
                    .addComponent(LbNitEmpresa))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lb2)
                    .addComponent(LbNombreEmpresa))
                .addContainerGap())
        );

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        lb4.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lb4.setText("Nombrel del equipo:");

        lb5.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lb5.setText("No. Resolución equipo:");

        lb6.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lb6.setText("Fecha Resolución equipo:");

        LbTerminal.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        LbTerminal.setText("No equipo");

        LbNoResolucion.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        LbNoResolucion.setText("No resolucion");

        LbFechaResolucion.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        LbFechaResolucion.setText("No fecha");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(155, 155, 155)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lb6)
                    .addComponent(lb5)
                    .addComponent(lb4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(LbNoResolucion)
                    .addComponent(LbFechaResolucion)
                    .addComponent(LbTerminal))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lb4)
                    .addComponent(LbTerminal))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(LbNoResolucion)
                    .addComponent(lb5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lb6)
                    .addComponent(LbFechaResolucion))
                .addContainerGap())
        );

        lb_ubicacion.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lb_ubicacion.setText("Dirección de la Base de datos:");

        jtf_ubicacion.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jtf_ubicacion.setText("localhost");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 670, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(213, 213, 213)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jtflogin_user, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 650, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(280, 280, 280)
                        .addComponent(btIngresar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(btSalir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(85, 85, 85)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel2)
                    .addComponent(lb_ubicacion))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jpfclave_user, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                    .addComponent(jtf_ubicacion))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(51, 51, 51)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jtflogin_user, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jpfclave_user, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lb_ubicacion)
                    .addComponent(jtf_ubicacion, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btIngresar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btSalir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 670, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 526, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSalirActionPerformed
        this.setVisible(false);
        System.exit(0);
    }//GEN-LAST:event_btSalirActionPerformed

    private void btIngresarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btIngresarActionPerformed
        FrmPrincipal frmPrincipal;
        String UrlDB;        
        Properties props = new Properties();
        if (("root".equals(jtflogin_user.getText())) && ("sistemas".equals(jpfclave_user.getText()))) {            
            UrlDB = "jdbc:mysql://"+jtf_ubicacion.getText()+":3306/dbsiscop-pos";            
            props.put("javax.persistence.jdbc.url", UrlDB);
            props.put("javax.persistence.jdbc.password", jpfclave_user.getText());
            props.put("javax.persistence.jdbc.driver","com.mysql.jdbc.Driver");
            props.put("javax.persistence.jdbc.user",jtflogin_user.getText());
            props.put("eclipselink.ddl-generation","create-tables");
    //        frmPrincipal = new FrmPrincipal(true, jtflogin_user.getText(), props);
            this.setVisible(false);            
    //        frmPrincipal.setVisible(true); //mostrando el formulario principal
                                                
        }else{
            UrlDB = "jdbc:mysql://"+urldb+":3306/dbsiscop-pos";            
            props.put("javax.persistence.jdbc.url", UrlDB);
            props.put("javax.persistence.jdbc.password", jpfclave_user.getText());
            props.put("javax.persistence.jdbc.driver","com.mysql.jdbc.Driver");
            props.put("javax.persistence.jdbc.user",jtflogin_user.getText());
            props.put("eclipselink.ddl-generation","create-tables");
    //        frmPrincipal = new FrmPrincipal(false, jtflogin_user.getText(), props);
            this.setVisible(false);
    //        frmPrincipal.setVisible(true); //mostrando el formulario principal
        }
            
        
        
    }//GEN-LAST:event_btIngresarActionPerformed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        System.exit(0);
    }//GEN-LAST:event_formWindowClosed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel LbFechaResolucion;
    private javax.swing.JLabel LbNitEmpresa;
    private javax.swing.JLabel LbNoLicencia;
    private javax.swing.JLabel LbNoResolucion;
    private javax.swing.JLabel LbNombreEmpresa;
    private javax.swing.JLabel LbTerminal;
    private javax.swing.JButton btIngresar;
    private javax.swing.JButton btSalir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JPasswordField jpfclave_user;
    private javax.swing.JPasswordField jtf_ubicacion;
    private javax.swing.JTextField jtflogin_user;
    private javax.swing.JLabel lb1;
    private javax.swing.JLabel lb2;
    private javax.swing.JLabel lb3;
    private javax.swing.JLabel lb4;
    private javax.swing.JLabel lb5;
    private javax.swing.JLabel lb6;
    private javax.swing.JLabel lb_ubicacion;
    // End of variables declaration//GEN-END:variables
}
