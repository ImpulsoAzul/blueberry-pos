/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package VISTA;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Timer;

/**
 *
 * @author pattyportillo
 */
public class FrmSplashScream extends javax.swing.JFrame {
    private Timer t;
    private ActionListener al;
    /**
     * Creates new form FrmSplashScream
     */
    public FrmSplashScream() {                        
        al = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (JpbSplash.getValue() < 100){
                    JpbSplash.setValue(JpbSplash.getValue() + 1); 
                    if (JpbSplash.getValue() == 25) Lbcarga.setText("Cargando módulos ...");
                    if (JpbSplash.getValue() == 50) Lbcarga.setText("Establaciendo parámetros de utilización ...");
                }else{
                    t.stop();
                    continuar();
                }   
                }                            
        };        
        t = new Timer(80, al); // 1s = 1000ms               
        t.start();
        this.setUndecorated(true);
        initComponents();                                    
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jSeparator1 = new javax.swing.JSeparator();
        JpbSplash = new javax.swing.JProgressBar();
        Lbcarga = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        jLabel2.setText("jLabel2");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setType(java.awt.Window.Type.UTILITY);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setPreferredSize(new java.awt.Dimension(800, 350));
        jPanel1.setLayout(null);

        jSeparator1.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator1);
        jSeparator1.setBounds(10, 270, 760, 10);

        JpbSplash.setBackground(new java.awt.Color(0, 204, 204));
        JpbSplash.setForeground(new java.awt.Color(0, 51, 204));
        jPanel1.add(JpbSplash);
        JpbSplash.setBounds(20, 170, 760, 30);

        Lbcarga.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        Lbcarga.setForeground(new java.awt.Color(255, 255, 255));
        Lbcarga.setText("Un momento ...");
        jPanel1.add(Lbcarga);
        Lbcarga.setBounds(20, 150, 320, 15);

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 48)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("SisCop - POS");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(450, 10, 320, 70);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("Fecha de publicacion: lunes 01 de septiembre de 2014");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(250, 230, 331, 15);

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("Derechos Reservados");
        jPanel1.add(jLabel5);
        jLabel5.setBounds(330, 250, 130, 15);

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/FondoSisCop-POS.jpg"))); // NOI18N
        jPanel1.add(jLabel6);
        jLabel6.setBounds(0, 0, 800, 180);

        jLabel7.setText("Propiedad intelectual y comercial SisCop \"Sistemas Corporativos\"");
        jPanel1.add(jLabel7);
        jLabel7.setBounds(10, 280, 390, 15);

        jLabel8.setText("Nit. 3282864-0            Tel. 54135579");
        jPanel1.add(jLabel8);
        jLabel8.setBounds(10, 300, 231, 30);

        jLabel9.setText("Chiquimula, Guatemala, Centro América");
        jPanel1.add(jLabel9);
        jLabel9.setBounds(10, 320, 270, 14);

        jLabel10.setText("email: siscop@outlook.com");
        jPanel1.add(jLabel10);
        jLabel10.setBounds(10, 290, 180, 20);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Version 1.0");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(360, 210, 100, 20);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void continuar(){
        this.setVisible(false);        
        FrmLogin frmLogin = new FrmLogin(); 
        frmLogin.setLocation(((Toolkit.getDefaultToolkit().getScreenSize().width)/2)-(frmLogin.getWidth()/2), (Toolkit.getDefaultToolkit().getScreenSize().height)/2-(frmLogin.getHeight()/2));
        frmLogin.setVisible(true); 
    }


    /**
     * @param args the command line arguments
     */
 
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JProgressBar JpbSplash;
    private javax.swing.JLabel Lbcarga;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator1;
    // End of variables declaration//GEN-END:variables
}
